package com.example.demo;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.entity.Board;
import com.example.demo.respository.BoardRepository;

@SpringBootTest
public class BoardRepositoryTest {
	
	@Autowired
	BoardRepository boardRepository;

	@Test
	public void 게시물등록1() { 
		Board board = new Board(0, "a", "ㅁ", "ㅁㅁ");
		boardRepository.save(board);

	}
	
	@Test
	public void 게시물등록2() {
		Board board = Board.builder()
						.title("2번글")
						.content("내용입니다")
						.writer("둘리")
						.build();
		boardRepository.save(board);
	}
	
	@Test
	public void 데이터목록조회() {
		List<Board> list = boardRepository.findAll();
		for(Board board : list) {
			System.out.println(board);
		}
	}
	@Test
	public void 게시물단건조회() {
		Optional<Board> result = boardRepository.findById(1);
		if(result.isPresent()) {
			Board board = result.get();
			System.out.println(board);
		}
	}
		@Test
		public void 게시물수정() {
			Optional<Board> result = boardRepository.findById(1);
			Board board = result.get();
			board.setContent("내용이수정되었습니다~");
			boardRepository.save(board);
	}
		@Test
		public void 게시물삭제() {
			boardRepository.deleteById(1);
		}
}
