package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.dto.BoardDTO;
import com.example.demo.service.BoardService;

@SpringBootTest
public class BoardServiceTest {
	
	@Autowired 
	BoardService boardService;
	
	@Test
	public void 게시물등록() {
		BoardDTO dto = new BoardDTO(0,"1번글","내용입니다","둘리",null,null);
//		BoardDTO dto = new BoardDTO(0,"2번글","내용입니다","또치",null,null);
		int no = boardService.register(dto);
		System.out.println("새로운 게시물 번호: " + no);
	}
	@Test
	public void 게시물수정() {
		BoardDTO dto =boardService.read(2);
		dto.setContent("내용이수정되었습니다~");
		boardService.modify(dto);
	

}
	@Test
	public void 게시물삭제() {
	 boardService.remove(2);
	}
}
